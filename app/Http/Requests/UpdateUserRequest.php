<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Rules\AlphaNumericUnderscore;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->route('user')->is(Auth::user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|alpha_num_underscore|min:3|max:20|unique:users,username,' . Auth::id()
        ];
    }

    public function messages()
    {
        return [
            'username.alpha_num_underscore' => 'Only letters, numbers and undersocre are allowed'
        ];
    }
}
